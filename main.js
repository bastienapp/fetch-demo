// Application Programming Interface
// un lien entre des données et un site

// fetch (ajax)
// Promises : .then()
// asynchrone : ça prend un certain temps avant d'avoir la réponse, mais ça ne bloque pas JavaScript

fetch("https://hp-api.onrender.com/api/characters")
  // quand l'API m'a envoyé la réponse, je la récupère
  .then((response) => response.json())
  // transformer la réponse de l'API dans un objet qu'on va pouvoir utilise
  .then((characterList) => {
    // pour chaque personnage de ma liste
    for (const eachCharacter of characterList) {
      // j'affiche le nom du personnage
      // console.log(eachCharacter.name);
    }
  });

/*
console.log("Avant la blague de Chuck");
fetch("https://api.chucknorris.io/jokes/random")
  .then((response) => response.json())
  .then((joke) => {
    console.log("Blague: " + joke.value);
    console.log("Vraiment après la blague !");
  });
console.log("Après la blague de Chuck (en fait non !)");
*/

// async / await : on attend la fin de chaque action asynchrone
async function callChuckJoke() {
  console.log("Avant la blague de Chuck");
  const response = await fetch("https://api.chucknorris.io/jokes/random");
  const joke = await response.json();
  console.log("Blague: " + joke.value);
  console.log("Après la blague de Chuck");
}

callChuckJoke();